﻿namespace BatToDuckyConverter
{
    using System.Collections.Generic;
    using System.IO;

    class Program
    {
        // Example usage: ./BatToDuckyConverter.exe "C:\commandlist.txt" "C:\duckycommands.txt"
        static void Main(string[] args)
        {
            var batchFilePath = args[0];
            var txtFilePath = args[1];

            var converter = new DuckyConverter();
            converter.ConvertBatchToDucky(batchFilePath, txtFilePath);
        }
    }

    class DuckyConverter
    {
        // INPUT: Takes a path to a file of shell commands (batPath) and a desired path and file name for the output file (txtPath)
        // OUPUT: A text file of rubber ducky commands that will start a cmd session, run each command, and exit the shell.
        public void ConvertBatchToDucky(string batPath, string txtPath)
        {
            // Boilerplate rubber ducky commands to enter and exit shell
            // Will be spliced onto the top and bottom of final command array before output to txt file.
            string[] commandsToStartCommandLineShell = { "DELAY 3000", "GUI R", "DELAY 500", "STRING cmd", "ENTER", "DELAY 500" };
            string[] commandsToExitCommandLineShell = { "STRING exit", "ENTER" };

            ////
            // "Ducky-ize" each command from the command list in the given file (batPath)
            ////
            
            // Array of strings - a list of shell commands from file
            var linesFromBATFile = File.ReadAllLines(batPath);
            
            // Add each line to the ducky script with commands for a 500 ms delay after pressing enter.
            var newDuckyLines = new List<string>();
            foreach (var lineFromBAT in linesFromBATFile)
            {
                // 500ms hardcoded below and above as time to wait for processing. I left it there for readability, but you'll
                // want to extract that out into a variable if you are going to tweak it.
                newDuckyLines.Add("STRING " + lineFromBAT);
                newDuckyLines.Add("ENTER");
                newDuckyLines.Add("DELAY 500");
            }

            ////
            // Build one huge array of ducky commands (tacking on opening and closing commands)
            ////
            
            // This array is the meat of the output, but we need to tack on the opening and closing commands
            var duckyLinesMadeFromBAT = newDuckyLines.ToArray();           

            // Create a new empty array with enough room to hold the opening commands, meat, and closing commands.
            var finalArraySize = 
                  commandsToStartCommandLineShell.Length 
                + duckyLinesMadeFromBAT.Length 
                + commandsToExitCommandLineShell.Length;
            var finalArray = new string[finalArraySize];
            
            // Splice the 3 smaller arrays into the super array.
            commandsToStartCommandLineShell.CopyTo(finalArray, 0);
            duckyLinesMadeFromBAT.CopyTo(finalArray, commandsToStartCommandLineShell.Length);
            commandsToExitCommandLineShell.CopyTo(finalArray, commandsToStartCommandLineShell.Length + duckyLinesMadeFromBAT.Length);

            ////
            // Dump the final array into a text file at the given path
            ////
            File.WriteAllLines(txtPath, finalArray);
        }
    }
}
